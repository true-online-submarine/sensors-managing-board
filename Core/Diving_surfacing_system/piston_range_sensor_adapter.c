#include "piston_range_sensor_adapter.h"
#include "main.h"
#include "vl53l0x.h"
#include "submarine_errors.h"
#include "logger.h"

extern I2C_HandleTypeDef hi2c1;

#define SENSOR_ADDR 0x52

static vl53l0x_ll_t vl53l0x_ll;
static vl53l0x_dev_t vl53l0x_dev;
static vl53l0x_range range;
static int ranging_sensor_int_flag;

static void vl53l0x_xshut_reset(void)
{
	HAL_GPIO_WritePin(XSHUT_GPIO_Port, XSHUT_Pin, GPIO_PIN_RESET);
}

static void vl53l0x_xshut_set(void)
{
	HAL_GPIO_WritePin(XSHUT_GPIO_Port, XSHUT_Pin, GPIO_PIN_SET);
}

static void vl53l0x_i2c_write_reg(uint8_t reg, uint8_t value)
{
	HAL_StatusTypeDef res = HAL_OK;
	uint8_t buf[2];

	buf[0] = reg;
	buf[1] = value;

	res = HAL_I2C_Master_Transmit(&hi2c1, SENSOR_ADDR, buf, 2, 100);
	SUB_ASSERT(res, SUB_DIVING_PISTON_RANGE_I2C_LL_ASSERT_13, 0);
}

static void vl53l0x_i2c_write_reg_16bit(uint8_t reg, uint16_t value)
{
	HAL_StatusTypeDef res = HAL_OK;
	uint8_t buf[3];

	buf[0] = reg;
	buf[1] = (uint8_t)value;
	buf[2] = (uint8_t)(value >> 8);

	res = HAL_I2C_Master_Transmit(&hi2c1, SENSOR_ADDR, buf, 3, 100);
	SUB_ASSERT(res, SUB_DIVING_PISTON_RANGE_I2C_LL_ASSERT_13, 0);
}

static void vl53l0x_i2c_write_reg_32bit(uint8_t reg, uint32_t value)
{
	HAL_StatusTypeDef res = HAL_OK;
	uint8_t buf[5];

	buf[0] = reg;
	buf[1] = (uint8_t)value;
	buf[2] = (uint8_t)(value >> 8);
	buf[3] = (uint8_t)(value >> 16);
	buf[4] = (uint8_t)(value >> 24);

	res = HAL_I2C_Master_Transmit(&hi2c1, SENSOR_ADDR, buf, 5, 100);
	SUB_ASSERT(res, SUB_DIVING_PISTON_RANGE_I2C_LL_ASSERT_13, 0);
}

static void vl53l0x_i2c_write_reg_multi(uint8_t reg, uint8_t *src_buf, size_t count)
{
	HAL_StatusTypeDef res = HAL_OK;
	uint8_t buf[32] = { 0 };

	/* hardcode, but I know, the lib use this function only to send 6 bytes max */
	if (count > sizeof(buf) - 1)
	{
		return;
	}

	buf[0] = reg;
	for (int i = 0; i < count; ++i)
	{
		buf[i + 1] = src_buf[i];
	}

	/* take in account reg byte */
	res = HAL_I2C_Master_Transmit(&hi2c1, SENSOR_ADDR, buf, count + 1, 100);
	SUB_ASSERT(res, SUB_DIVING_PISTON_RANGE_I2C_LL_ASSERT_13, 0);
}

static uint8_t vl53l0x_i2c_read_reg(uint8_t reg)
{
	HAL_StatusTypeDef res = HAL_OK;
	uint8_t ret;

	res = HAL_I2C_Master_Transmit(&hi2c1, SENSOR_ADDR, &reg, 1, 100);
	res += HAL_I2C_Master_Receive(&hi2c1, SENSOR_ADDR, &ret, 1, 100);
	SUB_ASSERT(res, SUB_DIVING_PISTON_RANGE_I2C_LL_ASSERT_13, 0);

	return ret;
}

static uint16_t vl53l0x_i2c_read_reg_16bit(uint8_t reg)
{
	HAL_StatusTypeDef res = HAL_OK;
	uint16_t ret;
	uint8_t recv[2];

	res = HAL_I2C_Master_Transmit(&hi2c1, SENSOR_ADDR, &reg, 1, 100);
	res += HAL_I2C_Master_Receive(&hi2c1, SENSOR_ADDR, recv, 2, 100);
	SUB_ASSERT(res, SUB_DIVING_PISTON_RANGE_I2C_LL_ASSERT_13, 0);

	ret = (uint16_t)recv[0] << 8;
	ret |= (uint16_t)recv[1];
	
	return ret;
}

static uint32_t vl53l0x_i2c_read_reg_32bit(uint8_t reg)
{
	HAL_StatusTypeDef res = HAL_OK;
	uint32_t ret;
	uint8_t recv[4];

	res = HAL_I2C_Master_Transmit(&hi2c1, SENSOR_ADDR, &reg, 1, 100);
	res += HAL_I2C_Master_Receive(&hi2c1, SENSOR_ADDR, recv, 4, 100);
	SUB_ASSERT(res, SUB_DIVING_PISTON_RANGE_I2C_LL_ASSERT_13, 0);

	ret = (uint32_t)recv[0] << 24;
	ret |= (uint32_t)recv[1] << 16;
	ret |= (uint32_t)recv[2] << 8;
	ret |= (uint32_t)recv[3];

	return ret;
}

static void vl53l0x_i2c_read_reg_multi(uint8_t reg, uint8_t *dst_buf, size_t count)
{
	HAL_StatusTypeDef res = HAL_OK;

	res = HAL_I2C_Master_Transmit(&hi2c1, SENSOR_ADDR, &reg, 1, 100);
	res += HAL_I2C_Master_Receive(&hi2c1, SENSOR_ADDR, dst_buf, count, 100);
	SUB_ASSERT(res, SUB_DIVING_PISTON_RANGE_I2C_LL_ASSERT_13, 0);
}

void piston_range_sensor_init(void)
{
	int res = 0;

	vl53l0x_ll.delay_ms = HAL_Delay;
	vl53l0x_ll.i2c_write_reg = vl53l0x_i2c_write_reg;
	vl53l0x_ll.i2c_write_reg_16bit = vl53l0x_i2c_write_reg_16bit;
	vl53l0x_ll.i2c_write_reg_32bit = vl53l0x_i2c_write_reg_32bit;
	vl53l0x_ll.i2c_read_reg = vl53l0x_i2c_read_reg;
	vl53l0x_ll.i2c_read_reg_16bit = vl53l0x_i2c_read_reg_16bit;
	vl53l0x_ll.i2c_read_reg_32bit = vl53l0x_i2c_read_reg_32bit;
	vl53l0x_ll.i2c_write_reg_multi = vl53l0x_i2c_write_reg_multi;
	vl53l0x_ll.i2c_read_reg_multi = vl53l0x_i2c_read_reg_multi;
	vl53l0x_ll.xshut_reset = vl53l0x_xshut_reset;
	vl53l0x_ll.xshut_set = vl53l0x_xshut_set;

	vl53l0x_dev.ll = &vl53l0x_ll;

	res = vl53l0x_init(&vl53l0x_dev);
	SUB_ASSERT(res, SUB_DIVING_PISTON_RANGE_SENSOR_ASSERT_10, 1);

	res = vl53l0x_set_measurement_mode(&vl53l0x_dev, VL53L0X_CONTINUOUS_RANGING, 0);
	SUB_ASSERT(res, SUB_DIVING_PISTON_RANGE_SENSOR_ASSERT_11, 1);

	res = vl53l0x_start_measurement(&vl53l0x_dev);
	SUB_ASSERT(res, SUB_DIVING_PISTON_RANGE_SENSOR_ASSERT_12, 1);
}

void piston_range_sensor_process(void)
{
    if (ranging_sensor_int_flag)
    {
        ranging_sensor_int_flag = 0;

        vl53l0x_get_range_mm_continuous(&vl53l0x_dev, &range);
        vl53l0x_clear_flag_gpio_interrupt(&vl53l0x_dev);

#if 0
		logger("%d\n", range.range_mm);
#endif
    }
}

void piston_range_sensor_isr(void)
{
    ranging_sensor_int_flag = 1;
}

uint16_t piston_range_sensor_get_pos_mm(void)
{
    return range.range_mm;
}
