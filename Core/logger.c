#include "logger.h"
#include "main.h"
#include <stdio.h>
#include <stdarg.h>

#define LOG_BUF_MAX_SIZE	128

extern UART_HandleTypeDef huart1;
static uint8_t log_buf[LOG_BUF_MAX_SIZE] = {0};

void logger(const char *format, ...)
{
	int32_t len = 0;

	va_list args;
	va_start(args, format);
	len = vsnprintf((char *)log_buf, LOG_BUF_MAX_SIZE, format, args);
	va_end(args);

	HAL_UART_Transmit(&huart1, log_buf, len, 100);
}
