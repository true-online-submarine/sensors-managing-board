#include "main.h"
#include "peripheral_init.h"

#include "piston_range_sensor_adapter.h"

int main(void)
{
	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* Configure the system clock */
	system_clock_config();

	/* Initialize all configured peripherals */
	gpio_init();
	uart1_init();
	i2c1_init();

	/* Diving and surfacing system init */
	piston_range_sensor_init();

	while (1)
	{
		piston_range_sensor_process();
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == VL53L0X_INT_Pin)
	{
		piston_range_sensor_isr();
	}
}
