#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f1xx_hal.h"

#define LED_Pin GPIO_PIN_13
#define LED_GPIO_Port GPIOC
#define VL53L0X_INT_Pin GPIO_PIN_4
#define VL53L0X_INT_GPIO_Port GPIOB
#define VL53L0X_INT_EXTI_IRQn EXTI4_IRQn
#define XSHUT_Pin GPIO_PIN_5
#define XSHUT_GPIO_Port GPIOB

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
