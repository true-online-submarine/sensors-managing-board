#ifndef __PERIPHERAL_INIT_H
#define __PERIPHERAL_INIT_H

#ifdef __cplusplus
extern "C" {
#endif

void system_clock_config(void);
void gpio_init(void);
void i2c1_init(void);
void uart1_init(void);

#ifdef __cplusplus
}
#endif

#endif /* __PERIPHERAL_INIT_H */
