#include "submarine_errors.h"
#include "logger.h"
#include "main.h"

void my_assert_func(int assert_num, int critical)
{
	int iter = 0;

	logger("[ERR]: %s ASSERT_%d\n", critical ? "critical" : "non critical", assert_num);

	if (critical)
	{
		while (1)
		{
			++iter;

			HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
			HAL_Delay(100);

			if (iter == 10)
			{
				HAL_NVIC_SystemReset();
			}
		}
	}
}
