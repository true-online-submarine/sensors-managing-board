#ifndef __SUBMARINE_ERRORS_H
#define __SUBMARINE_ERRORS_H

#ifdef __cplusplus
extern "C" {
#endif

void my_assert_func(int assert_num, int critical);

#define SUB_ASSERT(expr, assert_num, critical) \
	do { \
		if (expr) { \
			my_assert_func(assert_num, critical); \
		} \
	} while (0)

/* DO NOT CHANGE the order! */
typedef enum {
	SUB_INIT_CLOCK_ASSERT_0 = 0,  /* Init asserts */
	SUB_INIT_CLOCK_ASSERT_1,
	SUB_INIT_I2C1_ASSERT_2,
	SUB_INIT_UART1_ASSERT_3,
	SUB_INIT_UNUSED_ASSERT_4,
	SUB_INIT_UNUSED_ASSERT_5,
	SUB_INIT_UNUSED_ASSERT_6,
	SUB_INIT_UNUSED_ASSERT_7,
	SUB_INIT_UNUSED_ASSERT_8,
	SUB_INIT_UNUSED_ASSERT_9,
	SUB_DIVING_PISTON_RANGE_SENSOR_ASSERT_10 = 10, /* Diving and surfacing system asserts */
	SUB_DIVING_PISTON_RANGE_SENSOR_ASSERT_11,
	SUB_DIVING_PISTON_RANGE_SENSOR_ASSERT_12,
	SUB_DIVING_PISTON_RANGE_I2C_LL_ASSERT_13,
} submarine_errors_t;

#ifdef __cplusplus
}
#endif

#endif /* __SUBMARINE_ERRORS_H */
